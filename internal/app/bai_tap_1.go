package app

import "fmt"

func BaiTap1() {
	TinhTienLuongNhanVien()
	TinhTongGiaTriTrungBinh()
	QuyDoiTien()
	TinhDienTich()
	Tinh_Tong_2_Ky_So()
}

func TinhTienLuongNhanVien() {
	luong_1_ngay_lan := 10000
	so_ngay_lam := 30
	luong := 0

	luong = luong_1_ngay_lan * so_ngay_lam
	fmt.Println("Lương nhân viên = ", luong)
}

func TinhTongGiaTriTrungBinh() {
	num1 := 10
	num2 := 20
	num3 := 30
	num4 := 40
	num5 := 50

	tong_gia_tri_trung_binh := float64(num1+num2+num3+num4+num5) / 5
	fmt.Println("Tổng giá trị trung bình =", tong_gia_tri_trung_binh)
}

func QuyDoiTien() {
	gia_vnd := 235000
	so_luong_usd := 2
	gia_2_usd := 0

	gia_2_usd = gia_vnd * so_luong_usd
	fmt.Println("2 USD =", gia_2_usd)
}

func TinhDienTich() {
	chieu_dai := 5
	chieu_rong := 3

	chu_vi := 0
	dien_tich := 0

	chu_vi = (chieu_dai + chieu_rong) * 2
	dien_tich = chieu_dai + chieu_rong

	fmt.Println("Chu vi =", chu_vi)
	fmt.Println("Diện tích=", dien_tich)
}

func Tinh_Tong_2_Ky_So() {
	n := 50
	tong_2_ky_so := 0

	hang_chuc := n / 10
	don_vi := n % 10
	tong_2_ky_so = hang_chuc + don_vi

	fmt.Println("Tổng 2 ký số =", tong_2_ky_so)
}
