package bai_tap_1_if_else_input

import "fmt"

func Bai_Tap_Tinh_Chan_Le() {
	Tinh_Chan_Le()
}

func Tinh_Chan_Le() {
	var num_1, num_2, num_3 float64

	so_chan := 0
	so_le := 0

	fmt.Print("Nhập số thứ 1")
	fmt.Scanln(&num_1)

	fmt.Print("Nhập số thứ 2")
	fmt.Scanln(&num_2)

	fmt.Print("Nhập số thứ 3")
	fmt.Scanln(&num_3)

	if num_1 <= 0 || num_2 <= 0 || num_3 <= 0 {
		fmt.Println("Dữ liệu không hợp lệ")
	} else {
		if int(num_1)%2 == 0 {
			so_chan++
		}

		if int(num_2)%2 == 0 {
			so_chan++
		}

		if int(num_3)%2 == 0 {
			so_chan++
		}
		so_le = 3 - so_chan

		fmt.Printf("Số lượng số chẳn: %d\n", so_chan)
		fmt.Printf("Số lượng số lẻ: %d\n", so_le)
	}
}
