package bai_tap_1_input

import (
	"fmt"
	"strconv"
)

func Bai_Tap_Tinh_Luong() {
	tong_luong := TinhLuong()

	fmt.Println("Tổng lương =", strconv.FormatFloat(tong_luong, 'f', 2, 64))
}

func TinhLuong() float64 {
	var luong_1_ngay float64
	var so_ngay_lam float64

	fmt.Print("Nhập lương mỗi ngày:")
	fmt.Scanln(&luong_1_ngay)

	fmt.Print("Nhập số ngày làm:")
	fmt.Scanln(&so_ngay_lam)

	tong_luong := luong_1_ngay * so_ngay_lam
	return tong_luong
}
