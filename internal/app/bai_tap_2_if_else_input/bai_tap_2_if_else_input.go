package bai_tap_2_if_else_input

import "fmt"

func Bai_Tap_String_If_Else() {
	hello_message := Say_Hello()
	fmt.Println(hello_message)
}

func Say_Hello() string {
	var user string

	fmt.Print("Nhập tên user")
	fmt.Scanln(&user)

	var hello string

	if user == "Ba" {
		hello = "Xin chào ba"
	} else if user == "Mẹ" {
		hello = "Xin chào mẹ"
	} else if user == "Anh" {
		hello = "Xin chào anh"
	} else {
		hello = "Xin chào người lạ ơi"
	}
	return hello
}
