package bai_tap_2_input

import (
	"fmt"
	"strconv"
)

func Bai_Tap_2_Input() {
	vnd := QuyDoi()
	fmt.Println("Số tiền được quy đổi", vnd)
}

func QuyDoi() string {
	var usd float64
	fmt.Print("Nhập số tiền USD:")
	fmt.Scanln(&usd)

	vnd := usd * 23500
	vndFormatted := strconv.FormatFloat(vnd, 'f', 0, 64)
	return vndFormatted
}
