package bai_tap_3_if_else_input

import "fmt"

func Bai_Tap_Doan_Hinh_Tam_Giac(a, b, c float64) string {
	if a <= 0 || b <= 0 || c <= 0 {
		return "Dữ liệu không hợp lệ"
	} else if a == b && b == c {
		return "Tam giác đều"
	} else if a == b || a == c || b == c {
		return "Tam giác cân"
	} else if a*a == b*b+c*c || b*b == a*a+c*c || c*c == a*a+b*b {
		return "Tam giác vuông"
	} else {
		return "Loại tam giác khác"
	}
}

func Doan_Hinh_Tam_Giac() {
	var canh1, canh2, canh3 float64

	fmt.Print("Nhập độ dài cạnh 1:")
	fmt.Scan(&canh1)

	fmt.Print("Nhập độ dài cạnh 2:")
	fmt.Scan(&canh2)

	fmt.Print("Nhập độ dài cạnh 3:")
	fmt.Scan(&canh3)

	ket_qua := Bai_Tap_Doan_Hinh_Tam_Giac(canh1, canh2, canh3)
	fmt.Println("Kết quả:", ket_qua)
}
