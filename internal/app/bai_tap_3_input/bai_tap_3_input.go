package bai_tap_3_input

import "fmt"

func Bai_Tap_3_Input() {
	gia_tri_trung_binh := Tinh_Gia_Tri_Trung_Binh()
	fmt.Println("Tổng giá trị trung bình = ", gia_tri_trung_binh)
}

func Tinh_Gia_Tri_Trung_Binh() float64 {
	var num1 float64
	var num2 float64
	var num3 float64
	var num4 float64
	var num5 float64

	fmt.Print("Nhập số thứ 1")
	fmt.Scanln(&num1)

	fmt.Print("Nhập số thứ 2")
	fmt.Scanln(&num2)

	fmt.Print("Nhập số thứ 3")
	fmt.Scanln(&num3)

	fmt.Print("Nhập số thứ 4")
	fmt.Scanln(&num4)

	fmt.Print("Nhập số thứ 5")
	fmt.Scanln(&num5)

	gia_tri_trung_binh := (num1 + num2 + num3 + num4 + num5) / 5
	return gia_tri_trung_binh
}
