package bai_tap_4_input

import "fmt"

func Bai_Tap_4_Input() {
	dien_tich, chu_vi := Tinh_Chu_Vi_Dien_Tich()
	fmt.Printf("Diện tích: %.2f\n", dien_tich)
	fmt.Printf("Chu vi: %.2f\n", chu_vi)
}

func Tinh_Chu_Vi_Dien_Tich() (float64, float64) {
	var chieu_rong, chieu_dai float64

	fmt.Print("Nhập chiều rộng")
	fmt.Scanln(&chieu_rong)

	fmt.Print("Nhập chiều dài")
	fmt.Scanln(&chieu_dai)

	dien_tich := chieu_dai * chieu_rong
	chu_vi := (chieu_dai + chieu_rong) * 2
	return dien_tich, chu_vi
}
