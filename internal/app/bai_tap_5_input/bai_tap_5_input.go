package bai_tap_5_input

import "fmt"

func Bai_Tap_5_Input() {
	tong_2_ky_so := Tinh_Tong_2_Ky_So()
	fmt.Printf("Tổng hai ký số: %2.f\n", tong_2_ky_so)
}

func Tinh_Tong_2_Ky_So() float64 {
	var number float64

	fmt.Print("Nhập số:")
	fmt.Scanln(&number)

	hang_chuc := number / 10
	don_vi := int(number) % 10
	tong_2_ky_so := hang_chuc + float64(don_vi)
	return tong_2_ky_so
}
