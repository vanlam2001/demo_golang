package app

import "fmt"

func Demo_Array() {
	Hien_Thi_User()
}

// 1
func Array_Number() {
	// Khai báo một mảng kiểu init với kích thước cố định là 5
	var numbers [5]int

	// Gán giá trị cho các phẩn tử mảng
	numbers[0] = 10
	numbers[1] = 20
	numbers[2] = 30
	numbers[3] = 40
	numbers[4] = 50

	// Truy cập và hiển thị giá trị của các phần tử trong mảng

	fmt.Println("Mảng numbers:", numbers)

	// Khai báo và khởi tạo mảng kiểu string
	names := [3]string{"Alice", "Bob", "Charlie"}

	// Truy cập và hiển thị giá trị của các phần tử trong mảng names

	fmt.Println("Mảng names:", names)

	// Sử dụng vòng lặp để duyệt qua các phần tử mảng
	fmt.Println("Duyệt mảng numbers:")
	for i := 0; i < len(numbers); i++ {
		fmt.Println(numbers[i])
	}

	fmt.Println("Duyệt mảng names:")
	for _, name := range names {
		fmt.Println(name)
	}
}

// 2
func Array_Number_2() {
	// Khai báo một mảng kiểu init với kích thước cố định là 10
	var numbers [10]int

	numbers[0] = 10
	numbers[1] = 20
	numbers[2] = 30
	numbers[3] = 40
	numbers[4] = 50
	numbers[5] = 60
	numbers[6] = 70
	numbers[7] = 80
	numbers[8] = 90
	numbers[9] = 100

	// Sử dụng vòng lặp để duyệt qua các phần tử trong mảng
	fmt.Println("Duyệt mảng numbers:")
	for i := 0; i < len(numbers); i++ {
		fmt.Println(numbers[i])
	}
}

// 3
func Array_String() {
	var names [3]string

	names[0] = "Alcie"
	names[1] = "Bob"
	names[2] = "Charlie"

	fmt.Println("Duyệt mảng names:")
	for _, name := range names {
		fmt.Println(name)
	}
}

// 4

func Ting_Tong_Cac_Phan_Tu() {
	// Khai báo aray kiểu init với 5 phần tử

	numbers := [5]int{10, 20, 30, 40, 50}

	// Tính tổng các phẩn tử trong mảng
	sum := 0
	for _, num := range numbers {
		sum += num
	}

	fmt.Println("Mảng number:", numbers)
	fmt.Println("Tổng các phần tử trong mảng:", sum)
}

// 5

func In_Cac_Phan_Tu() {
	// Khai báo một arry kiểu string với 3 phần tử
	fruits := [3]string{"Apple", "Banana", "Orange"}

	fmt.Println("Mảng fruits:")
	for i := 0; i < len(fruits); i++ {
		fmt.Println((fruits[i]))
	}
}

// 6

type Hoc_Sinh struct {
	Name  string
	Age   int
	Grabe float64
}

func Hien_Thi_Thong_Tin_Doi_Tuong() {
	students := [3]Hoc_Sinh{
		{"Alice", 18, 9.5},
		{"Bob", 17, 8.6},
		{"Charlie", 19, 9.2},
	}

	fmt.Println("Mảng students:")
	for _, student := range students {
		fmt.Printf("Tên: %s, Tuổi: %d, Điểm: %.2f\n", student.Name, student.Age, student.Grabe)
	}
}

// 7
type User_List struct {
	FirstName string
	LastName  string
	Phone     string
}

func Hien_Thi_User() {
	// Khai báo và khởi tạo một mảng kiểu User_List
	var phoneBook [3]User_List

	// Gán giá trị người dùng trong danh bạ
	phoneBook[0] = User_List{
		FirstName: "Alice",
		LastName:  "Johnson",
		Phone:     "099799118",
	}

	phoneBook[1] = User_List{
		FirstName: "Bob",
		LastName:  "Smith",
		Phone:     "099766116",
	}

	phoneBook[2] = User_List{
		FirstName: "Charlie",
		LastName:  "Brown",
		Phone:     "099766117",
	}

	// In danh bạ điện thoại
	fmt.Println("Danh bạ điện thoại")
	for _, person := range phoneBook {
		fmt.Printf("Họ và tên: %s %s\n", person.FirstName, person.LastName)
		fmt.Printf("Số điện thoại: %s\n", person.Phone)
		fmt.Println("--------------")
	}
}
