package app

import (
	"fmt"

	"github.com/fatih/color"
)

func Demo_For_Lopp() {
	Result_For_Color()
}

func Demo_While() {
	i := 1

	for i < 6 {
		fmt.Println(i)
		i++
	}
	fmt.Println("i is no lonnger less than 6")
}

func Demo_Foor() {
	// Sử dụng vòng lặp fir để in các số từ 1 đến 1000

	for i := 1; i <= 1001; i++ {
		fmt.Println(i)
	}
}

func Result_For_Color() {
	coloredExForColor := Demo_For_Color()
	fmt.Println(coloredExForColor)
}

func Demo_For_Color() string {
	text := ""

	for text_1 := 0; text_1 <= 11; text_1++ {
		if text_1%2 == 0 {
			text += color.RedString("Số chẵn") + ""
		} else {
			text += color.BlueString("Số lẻ") + ""
		}
	}
	return text
}
