package app

import "fmt"

func Demo_If_Else() {
	Kiem_Tra_Do_Tuoi()
	Kiem_Tra_Nhiet_Do()
	If_Else_Kiem_Tra_Nhiet_Do()
	Chan_Le()
	Demo_Switch_Case()
}

func Kiem_Tra_Do_Tuoi() {
	age := 18

	if age >= 18 {
		fmt.Println("Bạn đã đủ tuổi để lái xe")
	} else {
		fmt.Println("Bạn chưa đủ tuổi lái xe")
	}
}

func Kiem_Tra_Nhiet_Do() {
	temperature := 19

	if temperature >= 30 {
		fmt.Println("Nhiệt độ cao, nắng nóng")
	} else if temperature >= 20 {
		fmt.Println("Nhiệt độ trung bình, thời tiết dễ chịu")
	} else {
		fmt.Println("Nhiệt độ thấp lạnh")
	}
}

func If_Else_Kiem_Tra_Nhiet_Do() {
	isSunny := true
	isHot := true

	if isSunny && isHot {
		fmt.Println("Trời nắng và nhiệt độ cao")
	} else if isSunny || isHot {
		fmt.Println("Trời nắng hoặc nhiệt độ cao (hoặc cả hai)")
	}
}

func Chan_Le() {
	if num := 10; num%2 == 0 {
		fmt.Println("Số chẵn")
	} else {
		fmt.Println("Số lẻ")
	}
}

func Demo_Switch_Case() {
	day := "Monday"

	switch day {
	case "Monday":
		fmt.Println("Thứ hai")
	case "Tuesday":
		fmt.Println("Thứ ba")
	case "Wednesday":
		fmt.Println("Thứ tư")
	default:
		fmt.Println("Ngày không xác định")
	}
}
