package app

import (
	"fmt"
	"math"
	"strconv"
)

func DemoNumber() {
	Number_1()
	Number_2()
	So_Hoc_Co_Ban()
	Demo_Math()
	Chuyen_Doi()
}

func Number_1() {
	number := 43
	fmt.Println(number)
}

func Number_2() {
	pi := 3.14159
	fmt.Println(pi)
}

// Các phép toán số học cơ bản
func So_Hoc_Co_Ban() {
	a := 10
	b := 5

	sum := a + b
	difference := a - b
	product := a * b
	quotient := a / b

	fmt.Printf("Tổng: %d\n", sum)
	fmt.Printf("Hiệu: %d\n", difference)
	fmt.Printf("Tích: %d\n", product)
	fmt.Printf("Thương: %d\n", quotient)
}

// Sử dụng thư viện math cho tính toán phức tạp hơn
func Demo_Math() {
	x := 2.0
	y := 3.0

	// Tính căng bậc hai
	spuareRoot := math.Sqrt(x)
	fmt.Printf("Căn bậc hai của %f là %f\n", x, spuareRoot)

	// Tích lũy thừa
	power := math.Pow(x, y)
	fmt.Printf("%f mũ %f là %f\n", x, y, power)
}

func Chuyen_Doi() {
	// Chuyển đổi chuỗi thành số
	str := "42"
	number, _ := strconv.Atoi(str)
	fmt.Printf("Chuỗi thành số: %d\n", number)

	// Chuyển đổi số thành chuỗi
	num := 123
	strFromNum := strconv.Itoa(num)
	fmt.Println("Số thành chuỗi:", strFromNum)

}
