package app

import "fmt"

func Demo_Object() {
	User()
}

// 1
type Person struct {
	FirstName string
	LastName  string
}

func Ex_Object() {
	person := Person{
		FirstName: "John",
		LastName:  "Doe",
	}

	// Truy cập và hiển thị giá trị của các trường trong đối tượng Person
	fmt.Println("FirstName:", person.FirstName)
	fmt.Println("LastName:", person.LastName)
}

// 2
type Student struct {
	ID       int
	Name     string
	Age      int
	Subjects []string
}

func Ex_Student__Object() {
	student_1 := Student{
		ID:       1,
		Name:     "Alice",
		Age:      18,
		Subjects: []string{"Math", "English"},
	}

	student_2 := Student{
		ID:       2,
		Name:     "Bob",
		Age:      17,
		Subjects: []string{"History", "Science"},
	}

	fmt.Println("Student 1:", student_1)
	fmt.Println("Student 2:", student_2)
}

// 3
type Car struct {
	Make  string
	Model string
	Year  int
}

func Ex_Car_Object() {
	car1 := Car{
		Make:  "Toyota",
		Model: "Camry",
		Year:  2021,
	}

	car2 := Car{
		Make:  "Honda",
		Model: "Civic",
		Year:  2020,
	}
	fmt.Println("Car 1:", car1)
	fmt.Println("Car 2:", car2)
}

// 4

type Product struct {
	ID      int
	Name    string
	Price   float64
	InStock bool
}

func Ex_Product_Object() {
	prodduct_1 := Product{
		ID:      1,
		Name:    "Laptop",
		Price:   999.99,
		InStock: true,
	}

	product_2 := Product{
		ID:      2,
		Name:    "Smartphone",
		Price:   599.99,
		InStock: false,
	}

	fmt.Println("Product 1:", prodduct_1)
	fmt.Println("Product 1:", product_2)
}

// 5

type Info_User struct {
	FirstName string
	LastName  string
	Age       int
	Address   Address
}

type Address struct {
	Street     string
	City       string
	PostalCode string
}

func User() {
	info := Info_User{
		FirstName: "Lam",
		LastName:  "Nguyen",
		Age:       20,
		Address: Address{
			Street:     "123 Main St",
			City:       "Ho Chi Minh City",
			PostalCode: "123",
		},
	}
	// Truy cập và hiển thị giá trị của các trường trong đối tượng info và Address
	fmt.Println("FirstName:", info.FirstName)
	fmt.Println("LastName:", info.LastName)
	fmt.Println("Age:", info.Age)
	fmt.Println("Address:", info.Address.Street)
	fmt.Println("City:", info.Address.City)
	fmt.Println("State:", info.Address.PostalCode)
}
