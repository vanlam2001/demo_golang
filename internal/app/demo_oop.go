package app

import "fmt"

func Demo_OOP() {
	OOP_User()
}

// Định nghĩa một struct có tên Person
type Thong_Tin_Nguoi_Dung struct {
	FirstName string
	LastName  string
	Age       int
}

// Định nghĩa một struct "Student" dẫn xuất từ Thong_Tin_Nguoi_Dung
type Thong_Tin_Hoc_Sinh struct {
	Thong_Tin_Nguoi_Dung
	StudentId string
	Courses   []string
}

// Phương thức cho đối tượng Person để hiển thị thông tin
func (p Thong_Tin_Nguoi_Dung) ShowInfo() {
	fmt.Printf("Họ và tên: %s %s\n", p.FirstName, p.LastName)
	fmt.Printf("Tuổi: %d\n", p.Age)
}

// Phương thức cho đối tượng Student để hiển thị thông tin, kế thừa từ Person và mở rộng thêm thông tin Student.
func (s Thong_Tin_Hoc_Sinh) ShowInfo() {
	s.Thong_Tin_Nguoi_Dung.ShowInfo()
	fmt.Printf("Mã số sinh viên: %s\n", s.StudentId)
	fmt.Printf("Các khoá học: %v\n", s.Courses)
}

func OOP_User() {
	// Tạo một đối tượng Thong_Tin_Nguoi_Dung
	person := Thong_Tin_Hoc_Sinh{
		Thong_Tin_Nguoi_Dung: Thong_Tin_Nguoi_Dung{
			FirstName: "Alice",
			LastName:  "Johnson",
			Age:       20,
		},
		StudentId: "123456",
		Courses:   []string{"Math", "English"},
	}

	// Gọi phương thức ShowInfo để hiển thị thông tin của đối tượng
	person.ShowInfo()
}
