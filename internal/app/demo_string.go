package app

import (
	"fmt"
	"strconv"
	"unicode/utf8"
)

func SayHello(name string) {
	fmt.Printf("Hello, %s!\n", name)
}

func SayGoodbye(name string) {
	fmt.Printf("Goodbye, %s!\n", name)
}

func DemoString() {
	SayHello("Alice")
	SayGoodbye("Bob")
	TestString()
	UniCode()
	DemoStringFor()
	InChuoi()
	CongChuoi()
	LayDoDaiChuoi()
	SoSanhChuoi()
	ChuyenDoiSoSangChuoi()
}

// Khai báo và sửa dụng chuỗi

func TestString() {
	// khai báo một chuỗi
	var str1 string = "Hello, World"

	// Khai báo một chuỗi ngắn gọn (short declaration)
	str2 := "This is a short string"

	fmt.Println(str1)
	fmt.Println(str2)
}

// Chuyển đổi chuỗi sang các mảng bytes và ngược lại
func UniCode() {
	str := "Xin chào" // Chuỗi có chứa ký tự đặc biệt
	// Chuyển đổi chuỗi thành một slice của byte
	bytes := []byte(str)
	fmt.Printf("Bytes: %v\n", bytes)

	// Chuyển đổi một slice của byte thành chuỗi
	strFromBytes := string(bytes)
	fmt.Printf("String from Bytes: %s\n", strFromBytes)

	// Đếm số ký tự Unicode trong chuỗi
	numChars := utf8.RuneCountInString(str)
	fmt.Printf("Number of characters: %d\n", numChars)
}

// Truy cập từng ký tự trong chuỗi
func DemoStringFor() {
	str := "Hello, World!"

	// Truy cập từng ký tự trong chuỗi
	for i := 0; i < len(str); i++ {
		fmt.Printf("Character at index %d: %c\n", i, str[i])
	}
}

// In chuỗi
func InChuoi() {
	message := "Hello, World!"
	fmt.Println(message)
}

// Kết hợp chuỗi

func CongChuoi() {
	greeting := "Hello"
	name := "Alice"

	// Kết hợp chuỗi
	message := greeting + "," + name + "!"
	fmt.Println(message)
}

func LayDoDaiChuoi() {
	text := "Chuỗi dài này có bao nhiêu ký tự"

	// Lấy chiều dài của chuỗi (số byte)
	length := len(text)
	fmt.Printf("Chiều dài của chuỗi là %d byte\n", length)

	// Lấy số ký tự Unicode trong chuỗi
	numChars := utf8.RuneCountInString(text)
	fmt.Printf("Số ký tự Unicode trong chuỗi là %d\n", numChars)
}

func SoSanhChuoi() {
	str1 := "apple"
	str2 := "apple"

	// So sánh chuỗi
	if str1 == str2 {
		fmt.Printf("Chuỗi giống nhau")
	} else {
		fmt.Printf("Chuỗi khác nhau")
	}
}

func ChuyenDoiSoSangChuoi() {
	number := 42

	// Chuyển đổi thành chuỗi
	str := strconv.Itoa(number)
	fmt.Println("Số thành chuỗi:", str)
}
