package app

import (
	"fmt"
	"unsafe"
)

func Pointer() {
	Demo_Pointer_Array_Number()
}

type Info_Student struct {
	FirstName string
	LastName  string
}

func Demo_Pointer_Number() {
	var x int = 42

	var pointerTox *int = &x

	fmt.Println("Giá trị của x:", *pointerTox)

	*pointerTox = 10
	fmt.Println("Giá trị của x sau khi thay đổi:", x)
}

func Demo_Float_Pointer() {
	y := 3.14
	var pointerTox *float64 = &y

	fmt.Println("Giá trị của y:", y)
	fmt.Println("Giá trị thông qua con trỏ:", *pointerTox)

	*pointerTox = 3.15
	fmt.Println("Giá trị của y sau khi thay đổi:", *pointerTox)
}

func Demo_Boolane_Pointer() {
	isTrue := true
	var pointerTox *bool = &isTrue

	fmt.Println("Giá trị của isTrue:", *&isTrue)
	fmt.Println("Giá trị thông qua con trỏ:", *pointerTox)

	*pointerTox = false
	fmt.Println("Giá trị của isTrue sau khi thay đổi", *pointerTox)

}

func Demo_Pointer_String() {
	s1 := "Hello world"
	var ptrToStr *string = &s1

	fmt.Println("Giá trị của s1:", *ptrToStr)

	*ptrToStr = "Test"
	fmt.Println("Giá trị của s1 sau khi thay đổi:", s1)
}

func Demo_Pointer_Cong_Chuoi() {
	string_1 := "Hello"
	string_2 := "World"

	var ptrToStr_1 *string = &string_1
	var ptrToStr_2 *string = &string_2

	combinedString := *ptrToStr_1 + " " + *ptrToStr_2
	fmt.Println(combinedString)
}

func Demo_Pointer_Total_Number() {
	number_1 := 1
	number_2 := 2

	var ptrToNumber_1 *int = &number_1
	var ptrToNumber_2 *int = &number_2

	result := *ptrToNumber_1 + *ptrToNumber_2
	fmt.Println(result)
}

func Demo_Pointer_Object() {
	info_student := Info_Student{
		FirstName: "Nguyen",
		LastName:  "A",
	}
	// Khai báo một con trỏ kiểu *Info_Student và gán địa chỉ của đối tượng Info_Student cho nó
	var ptrToInfoStudent *Info_Student = &info_student
	fmt.Println("FirstName:", ptrToInfoStudent.FirstName)
	fmt.Println("LastName:", ptrToInfoStudent.LastName)

	// Thay đổi giá trị của đối tượng thông qua con trỏ
	ptrToInfoStudent.FirstName = "Test"
	fmt.Println("Giá trị của FirstName sau khi thay đổi:", ptrToInfoStudent.FirstName)

	ptrToInfoStudent.LastName = "Ok"
	fmt.Println("Giá trị của LastName sau khi thay đổi:", ptrToInfoStudent.LastName)
}

func Demo_Pointer_Array_Number() {
	// Khai báo một mảng kiểu int có 5 phần tử
	arr := [5]int{1, 2, 3, 4, 5}

	// Khai báo một con trỏ kiểu *int và gán địa chỉ của phần tử đầu tiên của mảng cho nó
	var ptrToArray *int = &arr[0]

	// In ra giá trị của mảng thông qua con trỏ
	fmt.Println("Duyệt mảng numbers:")
	for i := 0; i < len(arr); i++ {
		fmt.Println(*ptrToArray)
		ptrToArray = (*int)(unsafe.Pointer(uintptr(unsafe.Pointer(ptrToArray)) + unsafe.Sizeof(arr[0])))
	}
}
